import models.*;
import algorithms.*;

public class MiniDuckSimulator {
	public static void main(String[] args) {
		Duck mallard = new MallardDuck();
		mallard.performQuack();
		mallard.performFly();
		
		System.out.println("-- testing dynamical set --");
		
		mallard.setQuackBehavior(new Squeak());
		mallard.performQuack();
	}
}